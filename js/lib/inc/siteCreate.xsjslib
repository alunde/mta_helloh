// $.import("mta_iot.js","crudCommon");
//$.import("crudCommon.xsjslib");


// Following logic from Thomas Jung
// http://scn.sap.com/thread/3447784

/**
@function Escape Special Characters in JSON strings
@param {string} input - Input String
@returns {string} the same string as the input but now escaped
*/
function escapeSpecialChars(input) {
          if(typeof(input) != 'undefined' && input != null)
          {
          return input
    .replace(/[\\]/g, '\\\\')
    .replace(/[\"]/g, '\\\"')
    .replace(/[\/]/g, '\\/')
    .replace(/[\b]/g, '\\b')
    .replace(/[\f]/g, '\\f')
    .replace(/[\n]/g, '\\n')
    .replace(/[\r]/g, '\\r')
    .replace(/[\t]/g, '\\t'); }
          else{
 
                    return "";
          }
}


/**
@function Converts any XSJS RecordSet object to a JSON Object
@param {object} rs - XSJS Record Set object
@param {optional String} rsName - name of the record set object in the JSON
@returns {object} JSON representation of the record set data
*/
function recordSetToJSON(rs,rsName){
          rsName = typeof rsName !== 'undefined' ? rsName : 'entries';
 
          var meta = rs.getMetaData();
          var colCount = meta.getColumnCount();
          var values=[];
          var table=[];
          var value="";
          while (rs.next()) {
          for (var i=1; i<=colCount; i++) {
                    value = '"'+meta.getColumnLabel(i)+'" : ';
               switch(meta.getColumnType(i)) {
               case $.db.types.VARCHAR:
               case $.db.types.CHAR:
                    value += '"'+ escapeSpecialChars(rs.getString(i))+'"';
                    break;
               case $.db.types.NVARCHAR:
               case $.db.types.NCHAR:
               case $.db.types.SHORTTEXT:
                    value += '"'+escapeSpecialChars(rs.getNString(i))+'"';
                    break;
               case $.db.types.TINYINT:
               case $.db.types.SMALLINT:
               case $.db.types.INT:
               case $.db.types.BIGINT:
                    value += rs.getInteger(i);
                    break;
               case $.db.types.DOUBLE:
                    value += rs.getDouble(i);
                    break;
               case $.db.types.DECIMAL:
                    value += rs.getDecimal(i);
                    break;
               case $.db.types.REAL:
                    value += rs.getReal(i);
                    break;
               case $.db.types.NCLOB:
               case $.db.types.TEXT:
                    value += '"'+ escapeSpecialChars(rs.getNClob(i))+'"';
                    break;
               case $.db.types.CLOB:
                    value += '"'+ escapeSpecialChars(rs.getClob(i))+'"';
                    break;                   
               case $.db.types.BLOB:
                          value += '"'+ $.util.convert.encodeBase64(rs.getBlob(i))+'"';
                    break;                   
               case $.db.types.DATE:
                    value += '"'+rs.getDate(i)+'"';
                    break;
               case $.db.types.TIME:
                    value += '"'+rs.getTime(i)+'"';
                    break;
               case $.db.types.TIMESTAMP:
                    value += '"' + (rs.getTimestamp(i)).getTime() + '"';
                    break;
               case $.db.types.SECONDDATE:
                    value += '"'+rs.getSeconddate(i)+'"';
                    break;
               default:
                    value += '"'+escapeSpecialChars(rs.getString(i))+'"';
               }
               values.push(value);
               }
             table.push('{'+values+'}');
          }
          return           JSON.parse('{"'+ rsName +'" : [' + table          +']}');
 
}

/**
 * @param {connection} Connection - The SQL connection used in the OData request
 * @param {beforeTableName} String - The name of a temporary table with the single entyr before the operation  (UPDATE and DELETE events only)
 * @param {afterTableName} String - The name of a temporary table with the single entry after the operation (CREATE and UPDATE events only)
**/


function siteCreate(param) {
    var after = param.afterTableName;
    
    //Get Input New Record Values
    var pStmt = param.connection.prepareStatement('select * from "' + after + '"');
    var Data = recordSetToJSON(pStmt.executeQuery(), 'Details');
    pStmt.close();
   
// 01 "tempId" INTEGER CS_INT NOT NULL , 
// 02 "tempVal" INTEGER CS_INT NOT NULL , 
// 03 "ts" LONGDATE CS_LONGDATE NOT NULL , 
// 04 "created" LONGDATE CS_LONGDATE NOT NULL , 

    
    var field1 = parseInt(Data.Details[0].siteId);
    
    var field2 = parseInt(Data.Details[0].GlobalRank);
    var field3 = parseInt(Data.Details[0].TldRank);
    var field4 = Data.Details[0].Domain;
    var field5 = Data.Details[0].TLD;


    //Validate Parameters
    // if (!validateField4Empty(field4)) {
    //     throw 'Invalid direction for ' + field4 + '';
    // }

    for (var i=0; i<2; i++) {

        if (i<1) {
            pStmt = param.connection.prepareStatement('insert into "mta_helloh.db::mm.sites"("GlobalRank","TldRank","Domain","TLD") values(?,?,?,?)');
        }
        else {
            pStmt = param.connection.prepareStatement('TRUNCATE TABLE "' + after + '" ');
            pStmt.executeUpdate();
            pStmt.close();
            
            // Need to figure out how to retrieve the last siteId index created.  Just returning 0 for now.
            
            pStmt = param.connection.prepareStatement('insert into "' + after + '"("siteId","GlobalRank","TldRank","Domain","TLD") values(0,?,?,?,?)');
        }
        
        // pStmt.setNull(1);
        pStmt.setInteger(1, field2);
        pStmt.setInteger(2, field3);
        pStmt.setString(3, field4);
        pStmt.setString(4, field5);

        pStmt.executeUpdate();
        pStmt.close();
    }
}

function validateField4Empty(the_field) {
    if (the_field === '') {
        return false;
    }
    else {
        return true;
    }
}


