 THE PURPOSE OF THIS EXAMPLE IS TO SHOW HOW EASY TO CREATE YOUR OWN SENDER TOOL TO SEND DATA TO STREAMING LITE. THIS IS NOT INTENDED FOR A
 FULL FLEDGE SIGNAL SENDER TOOL. 

 This example only works with Lite. Makefile is provided to compile the example. You compile doing: make -f Makefile.lite

 This example expects you to start Streaming Lite with sensorproject.ccx file which is included in this folder. Follow these steps:

 1. set STREAMING_HOME to the folder where you have installed the Streaming Lite
 2. $STREAMING_HOME/bin/streamingproject --ccx sensorproject.ccx --command-port 9094 & (or leave off the & to keep it in the foreground)
 3. Once above project starts then send temperature sensor data:
		./sendsignal --command="echo 70" --type="temperature" --unit=F 
 4. You can subscribe the signalout stream of signals project by doing following:
		$STREAMING_HOME/bin/streamingsubscribe -p localhost:9094 -s SENSOR

 Note: To list all available options, do following: ./sendsignals --help
       sendsignal executes the command provided as part of the --command argument to generate a number. You can use "echo 70" to geneate value 70
       or use any executable such as sensor driver executable to get the temperature reading.

 What is expected:
    - by default this expects Streaming Lite is running in localhost:9094. You can change this with command line option "uri"
    - by default this expects there is no authentication in Streaming Lite. If you started Lite with -V option then use "creds" 
      options to provide the username:password
    - by default it expects a input stream name called signals. You can also supply this through command line using "stream"
    - by default at minimum it expects input stream will have two columns: 1st one is long and second one is decimal. 
    - it generates a timestamp in seconds for first column automatically and second column is populated with the data provided in the "command" option.
      Data for rest of the columns need to be provided as part of the command line argument and are optional.
    - this example uses the following schema for input stream:

		(ts long, value decimal(18, 8), uuid string, type string, unit string, 
		 longitude string, latitude string, location string, other string)

	- this example does not support SSL.

 Note: sensorsimulator is an random number generator within a range (default: 1 to 120) which user can provide as arguments. You can use this 
 executable to simulate a sensor. You can use this as follows to generate random number between 20 and 90: 
		
	./sendsignals --command="./sensorsimulator 20 90" --type="temperature" --unit=F --repeat=-1
