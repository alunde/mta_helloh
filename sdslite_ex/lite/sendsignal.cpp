/*
* THE PURPOSE OF THIS EXAMPLE IS TO SHOW HOW EASY TO CREATE YOUR OWN SENDER TOOL TO STREAMING LITE. THIS IS NOT INTENDED FOR A
* FULL FLEDGE SIGNAL SENDER TOOL. 
*
* This example only works with Lite. Makefile is provided to compile this. Use g++ compiler.
* This example expects you to start Streaming Lite with singals.ccx file following way before you run this:
*
* 1. set STREAMING_HOME to the folder where you have installed the Streaming Lite
* 2. $STREAMING_HOME/bin/streamingproject --ccx signals.ccx --command-port 9230 &
* 3. Once above project starts then send temperature sensor data:
		./sendsignals --command="echo 70" --type="temperature" --unit=F
* 4. You can subscribe the signalout stream of signals project by doing following:
*		$STREAMING_HOME/bin/streamingsubscribe --command-port 9230 signalout
*
*	NOTE: sendsignal executes the command provided as part of the --command argument to generate a number. You can use "echo 70" to geneate value 70
*		  or use any executable such as sensor driver executable to generate number. 

* What this example does:
*	process_comand_line(...) and validate_options(...) are for reading command line options.
*   sendsignal(...) function actually sends the message to Lite.
*
* What is expected:
*   - by default this expects Streaming Lite is running in localhost:9230. You can change this with command line option "uri"
*   - by default this expects there is no authentication in Streaming Lite. If you started Lite with -V option then use "creds" 
*     options to provide the username:password
*   - by default it expects a input stream name called signals. You supply this through command line using "stream"
*   - by default at minimum it expects input stream will have two columns: 1st one is long and second one is decimal.
*	- it generates a timestamp in seconds for first column automatically and second column is populated with the data provided in the "command" option.
*	  Data for rest of the column need to be provided as part of the command line argument.
*	- this example uses the following schema for input stream:
*
*		(ts long, value decimal(18, 8), uuid string, type string, unit string, 
*		 longitude string, latitude string, location string, other string)
*
*/

#include <string>
#include <iostream>
#include <stdlib.h>
#include <getopt.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>

#include "sdk/esp_sdk.h"
#include "sdk/esp_error.h"
#include "sdk/esp_uri.h"
#include "sdk/esp_credentials.h"
#include "sdk/esp_project.h"
#include "sdk/esp_stream.h"
#include "sdk/esp_publisher.h"

#define BUFFSIZE 128

#define OPT_URI  1000 
#define OPT_CRED 1001 
#define OPT_COMM 1002 
#define OPT_REAP 1003 
#define OPT_INTR 1004 
#define OPT_TYPE 1005 
#define OPT_UNIT 1006 
#define OPT_UUID 1007 
#define OPT_LNGI 1008 
#define OPT_LATI 1009 
#define OPT_LOCA 1010 
#define OPT_OTHR 1011 
#define OPT_STRM 1012 
#define OPT_HELP 1013 

using namespace std;

struct options_t
{
    string uri;
    string stream;
    string creds;
    string command;
    int repeat;
    int interval;
    string type;
    string unit;
    string uuid;
    string longitude;
    string latitude;
    string location;
    string other;
};

struct options_t  gopts;

static struct option long_options[] = 
{
    { "uri", 1, 0, OPT_URI },
    { "stream", 1, 0, OPT_STRM },
    { "cred", 1, 0, OPT_CRED },
    { "command", 1, 0, OPT_COMM },
    { "repeat", 1, 0, OPT_REAP },
    { "interval", 1, 0, OPT_INTR },
    { "type", 1, 0, OPT_TYPE },
    { "unit", 1, 0, OPT_UNIT },
    { "uuid", 1, 0, OPT_UUID },
    { "longitude", 1, 0, OPT_LNGI },
    { "latitude", 1, 0, OPT_LATI },
    { "location", 1, 0, OPT_LOCA },
    { "other", 1, 0, OPT_OTHR },
    { "help", 1, 0, OPT_HELP },
    { 0, 0, 0, 0}
};

void sendsignal();

void show_help()
{
    cout << "\nThis example tool only works for Streaming Lite. Use this to send sensor signals to default project signal.ccx. It can also be used to send signals to any stream provided the stream has following schema: \n\n";
    cout << "\t(ts long, \n\tvalue decimal(18, 8), \n\tuuid string, \n\ttype string, \n\tunit string, \n\tlongitude string, \n\tlatitude string, \n\tlocation string, \n\tother string)\n\n";
    cout << "Usage : sendsignal [options]\n";
    cout << "      --uri\t\t: host:port for Streaming Lite (default localhost:9230)\n";
    cout << "      --stream\t\t: stream name that you want to publish (default signals)\n";
    cout << "      --cred\t\t: user:password for Streaming Lite (default none)\n";
    cout << "      --command\t\t: command to execute to produce signal i.e. a number which is sent to value column of the stream (required)\n";
    cout << "      --repeat\t\t: number of times command is executed (default 1), -1 is for infinite\n";
    cout << "      --interval\t: repeat interval in seconds (default 5)\n";
    cout << "      --type\t\t: type of the signal the command generates, e.g., temperature, distance etc. (default NULL)\n"; 
    cout << "      --unit\t\t: unit of the signal that is being sent, e.g., F, C, Feet etc.  (default NULL)\n";
    cout << "      --uuid\t\t: id for the device (default NULL)\n";
    cout << "      --longitude\t: longitude of the location (default NULL)\n";
    cout << "      --latitude\t: latitude of the location (default NULL)\n";
    cout << "      --location\t: location information (default NULL\n";
    cout << "      --other\t\t: any other information (default NULL)\n";
    cout << "      --help\t\t: show what you are reading now\n\n";
    cout << "Example :\n";
    cout << "\tsendsignal --command=\"./sensorsimulator\" \n";
    cout << "\tsendsignal --command=\"echo 50\" \n";
    cout << "\tsendsignal --command=\"sensorsimulator 20 90\" --type=temperature --unit=F --repeat=100\n";
	cout << "\n";
}

int process_command_line(int argc, char * argv[])
{
    int index=0;
    int c;

    // defaults
    gopts.uri = "localhost:9094";
    gopts.stream = "SENSOR";
    gopts.creds = "";
    gopts.command = "";
    gopts.repeat = 1;
    gopts.interval = 5;
    gopts.type = "";
    gopts.unit = "";
    gopts.uuid = "";
    gopts.longitude = "";
    gopts.latitude = "";
    gopts.location = "";
    gopts.other = "";

    while ( (c = getopt_long(argc, argv, "", long_options, &index)) != -1) 
	{
        switch (c) {
        case OPT_URI:
            gopts.uri = optarg;
            break;
        case OPT_STRM:
            gopts.stream = optarg;
            break;
        case OPT_CRED:
            gopts.creds = optarg;
            break;
        case OPT_COMM:
            gopts.command = optarg;
            break;
        case OPT_REAP:
            gopts.repeat = atoi(optarg);
            break;
        case OPT_INTR:
            gopts.interval = atoi(optarg);
            break;
        case OPT_TYPE:
            gopts.type = optarg;
            break;
        case OPT_UNIT:
            gopts.unit = optarg;
            break;
        case OPT_UUID:
            gopts.uuid = optarg;
            break;
        case OPT_LNGI:
            gopts.longitude = optarg;
            break;
        case OPT_LATI:
            gopts.latitude = optarg;
            break;
        case OPT_LOCA:
            gopts.location = optarg;
            break;
        case OPT_OTHR:
            gopts.other = optarg;
            break;
        case OPT_HELP:
        case '?':
            show_help();
            return 1;
        }
    }
    return 0;
}

int validate_options(options_t * opts)
{
    int ret = 0;

    if (opts->command.empty()) 
	{
        cout << "missing command option\n";
        ret = 1;
    }
    return ret;
}

int main(int argc, char * argv[])
{
        if (process_command_line(argc, argv))
        {
                return 0;
        }

        if ( validate_options(&gopts) )
        {
        	return 0;
        }

		sendsignal();
}




EspError        * g_error = NULL;
EspCredentials  * g_creds = NULL;


void print_error_and_exit(EspError * streamingError, int line) {
	printf("An error was encountered on line %d\n", line);
	printf("%s", esp_error_get_message(streamingError));
	printf("\nExiting\n");
    if (g_creds) esp_credentials_free(g_creds, g_error);
    if (g_error) esp_error_free(g_error);
    exit(1);
}

// Execute a command and return result as string.
// In this example the command should only generate one number. 

string exec(char const* cmd)
{
    FILE* pipe = popen(cmd, "r");
    if (!pipe) return "ERROR";
    char buffer[128];
    string result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL)
            result += buffer;
    }
    pclose(pipe);
    return result;
}

void  sendsignal()
{
	int rc = 0;
	char buff[BUFFSIZE];

	EspError * g_error = esp_error_create();

	rc = esp_sdk_start(g_error);
	if (rc != 0) print_error_and_exit(g_error, __LINE__);

	g_creds = esp_credentials_create(ESP_CREDENTIALS_USER_PASSWORD, g_error);
	if (NULL == g_creds) print_error_and_exit(g_error, __LINE__);

	string user ="";
	string pw ="";
	int pos = gopts.creds.find(":");
	if (pos > 0)
	{
		user = gopts.creds.substr(0, pos);
		if (gopts.creds.length() > pos +1)
		{
			pw = gopts.creds.substr(pos+1);
		}
	}

	esp_credentials_set_user(g_creds, user.c_str(), g_error);
	esp_credentials_set_password(g_creds, pw.c_str(), g_error);

	string host = "localhost";
	int port  = 9094;

	string tmp = "localhost:9094";	
	if (tmp.compare(gopts.uri) != 0 )
	{
		pos = gopts.uri.find(":");
		if (pos > 0)
		{
			host = gopts.uri.substr(0, pos);
			if (gopts.uri.length() > pos + 1)
			{
				port = atoi((gopts.uri.substr(pos + 1)).c_str());
			}
		}
	}

	cout << "\nStart sending signals to project server running at " << host << ":" << port << "\n";

	EspProject * project = esp_project_get_standalone(host.c_str(), port, g_creds, NULL, g_error);
	if (NULL == project) print_error_and_exit(g_error, __LINE__);

	rc = esp_project_connect(project, g_error);
	if (rc != 0) print_error_and_exit(g_error, __LINE__);

	// Once we have retrieved the project we do not need the EspCredentials and EspUri
	// objects since we are not going to reuse them. Need to free them
	esp_credentials_free(g_creds, g_error);
	g_creds = NULL;

	const EspStream * stream = esp_project_get_stream(project, gopts.stream.c_str(), g_error);
	if (NULL == stream) print_error_and_exit(g_error, __LINE__);

	EspPublisher * publisher = esp_project_create_publisher(project, NULL, g_error);
	if (NULL == publisher) print_error_and_exit(g_error, __LINE__);

	rc = esp_publisher_connect(publisher, g_error);
	if (rc != 0) print_error_and_exit(g_error, __LINE__);

	EspMessageWriter * message = esp_publisher_get_writer(publisher, stream, g_error);
	if (NULL == message) print_error_and_exit(g_error, __LINE__);

	EspRelativeRowWriter * row = esp_message_writer_get_relative_rowwriter(message, g_error);
	if (NULL == row) print_error_and_exit(g_error, __LINE__);

	//repeat
	if (gopts.repeat == 0 )
	{
		gopts.repeat = 1;
	}

	do
	{

		if ( gopts.repeat != -1)
		{
			gopts.repeat --;
		}

		rc = esp_relative_rowwriter_start_row(row, g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		rc = esp_relative_rowwriter_set_operation(row, ESP_STREAM_OP_INSERT, g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 1 a LONG
		rc = esp_relative_rowwriter_set_long(row, (long)time(NULL), g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 2 a DECIMAL
		string val = exec(gopts.command.c_str());
		double dv = atof(val.c_str());

		if (val.length() > 0 && val.at(0) == '0')
		{
			// this is a valid value - it could be 0 or 0.0
		}
		else if (0.0 == dv )
		{
			printf("Bad data to send, skip sending....\n");
			// wait for interval..
			if ((gopts.repeat == -1 || gopts.repeat > 0) && gopts.interval > 0)
			{
				sleep( gopts.interval);
			}
			continue;
		}

		EspFixedDecimal *decimalvalue = esp_fixeddecimal_create_prestring((exec(gopts.command.c_str())).c_str(), g_error);
		rc = esp_relative_rowwriter_set_fixeddecimal(row, decimalvalue, g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 3 a STRING
		//sprintf(buff, "Hello%d", i);
		rc = esp_relative_rowwriter_set_string(row, gopts.uuid.c_str(), g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 4 a STRING
		rc = esp_relative_rowwriter_set_string(row, gopts.type.c_str(), g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 5 a STRING
		rc = esp_relative_rowwriter_set_string(row, gopts.unit.c_str(), g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 6 a STRING
		rc = esp_relative_rowwriter_set_string(row, gopts.longitude.c_str(), g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 7 a STRING
		rc = esp_relative_rowwriter_set_string(row, gopts.latitude.c_str(), g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 8 a STRING
		rc = esp_relative_rowwriter_set_string(row, gopts.location.c_str(), g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		// column 9 a STRING
		rc = esp_relative_rowwriter_set_string(row, gopts.other.c_str(), g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);


		rc = esp_relative_rowwriter_end_row(row, g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		rc = esp_publisher_publish(publisher, message, g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);
		
		rc = esp_publisher_commit(publisher, g_error);
		if (rc != 0) print_error_and_exit(g_error, __LINE__);

		printf("Messages published successfully\n");

		// wait for interval..
		if ((gopts.repeat == -1 || gopts.repeat > 0) && gopts.interval > 0)
		{
			sleep( gopts.interval);
		}
	} 
	while ((gopts.repeat == -1 || gopts.repeat > 0) && gopts.interval > 0);

	esp_error_free(g_error);
}
