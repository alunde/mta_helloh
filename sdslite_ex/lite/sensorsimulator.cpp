#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>       

/*
* This program generates a random number within a range. Range can be specified through command line. If no range is specified then it
* it assumes lower range 1 and upper range 120.
* 
* This program can be used as a "command" argument for sendsensor executable to send a simulated temperature reading. Please check the readme 
* for detail.
*
*/

int main (int argc, char* argv[])
{

  /* initialize random seed: */
  srand (time(NULL));

  int low = 1;
  int max = 120;

  if (argc > 1)
  {
	low = atoi(argv[1]);
  }

  if (argc > 2)
  {
	max = atoi(argv[2]);
  }

  int r = rand() % max + low;
  if (r > max)
  {
    r = max;
  }
  printf("%d", r);
  return 0;
}
