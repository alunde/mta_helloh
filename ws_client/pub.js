var WebSocketClient = require('websocket').client;
var client = new WebSocketClient();
var tunnel = require('tunnel');
var os = require('os');

var tunnelingAgent = tunnel.httpOverHttp({
  proxy: {
    host: 'localhost', // Defaults to 'localhost'
    port: 8888 // Defaults to 80
  }
});

var requestOptions = {
    agent: tunnelingAgent
};

var headers = {
	'Authorization': 'Basic U1lTVEVNOldlbGNvbWUx'
};

//client.connect('ws://xsadv.sfphcp.com:9093/1/workspaces/default/projects/hellot/publisher', null, null, null, requestOptions);

var client = new WebSocketClient();
 
client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});
 
client.on('httpResponse', function(response, webSocketClient) {
    console.log('httpResponse: ' + response.toString());
});
 
client.on('connect', function(connection) {

    console.log('WebSocket Client Connected to: ' + connection.remoteAddress);

    connection.on('ping', function(cancel, data) {
        console.log("Ping: ");
    });

    connection.on('pong', function(data) {
        console.log("Pong: ");
    });

    connection.on('frame', function(webSocketFrame) {
        console.log("Frame: ");
    });

    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });

    connection.on('close', function(reasonCode,description) {
        console.log('Connection Closed:' + reasonCode + ' ' + description);
    });

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log("Received: '" + message.utf8Data + "'");
        }
        else if (message.type === 'binary') {
            console.log("Received: '" + length(message.binaryDataBuffer) + " Bytes'");
        }
        else {
            console.log("Unknown Msg type.");
        }
    });
    
    function sendNumber() {
        if (connection.connected) {
            var number = Math.round(Math.random() * 0xFFFFFF);
            connection.sendUTF(number.toString());
            setTimeout(sendNumber, 1000);
        }
    }
    
var delay = 0;
var count = 1000000;
var index = 0;
var start = null;
var elapsed = null;

function sendData() {

	//var number = Math.round(Math.random() * 0xFFFFFF);
	var number = os.freemem();

	var req = "";

	req += '{ "stream":"NEWSTREAM",'; 
	req += '"data": [';

	//req += '{ "ESP_OPS":"i", "tempVal":' + number + ' }';
	req += '[ "i",' + number + ']';

	req += '] }';

	if (index == 0) {
		start = new Date().getTime();
		console.log('\nBegin Sending ' + count.toLocaleString() + ' events with ' + delay + 'ms delay.\n');
	}

	if (connection.connected) {
		if (((index % 10000) == 0) && (index > 0)) {
			process.stdout.write(":" + index.toLocaleString() + " " + ((index/count)*100).toFixed(2) + "% \n");
		}
		if ((index % 1000) == 0) {
			process.stdout.write(".");
		}
		//	console.log('Sending: ' + req);
		connection.sendUTF(req);
		if (index < count) {
			setTimeout(sendData, delay);
		}
	}

	index++;

	if (index == count) {

		process.stdout.write(":" + index.toLocaleString() + " " + ((index/count)*100).toFixed(2) + "% \n");
	
		elapsed = new Date().getTime() - start;
		console.log('\nEnd Sending ' + count.toLocaleString() + ' events with ' + delay.toLocaleString() + 'ms delay in ' + (elapsed/1000).toFixed(3) + ' secs elapsed (' + (elapsed / 60000).toFixed(2) + ' mins) = ' + (elapsed / count) + ' ms per event.\n');
		connection.close();
	}

}



    //sendNumber();

    function requestPrivilege() {
	var req = "";
	req += '[ ';
	req += '{ "Authorization": "Basic U1lTVEVNOldlbGNvbWUx" }, ';
	req += '{ "privilege":"write", "resourceType":"stream", "resource":"default/hellot/NEWSTREAM" }';
	req += ' ]';

        if (connection.connected) {
   	    console.log('Sending: ' + req);
            connection.sendUTF(req);
            setTimeout(sendData, 500);
        }
	else {
    		console.log('Request not sent. No connection.');
	}
    }

    //console.log('Requesting Privilege');
    //requestPrivilege();
    setTimeout(requestPrivilege, 500);
});
 
client.connect('ws://xsadv.sfphcp.com:9093/1/workspaces/default/projects/hellot/publisher/', null, null, null, null);

