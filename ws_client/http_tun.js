var tunnel = require('tunnel');
var http = require('http');

var tunnelingAgent = tunnel.httpOverHttp({
  maxSockets: 5, // Defaults to 5

  proxy: { // Proxy settings
    host: 'localhost', // Defaults to 'localhost'
    port: 8888, // Defaults to 80
    //localAddress: localAddress, // Local interface if necessary

    // Basic authorization for proxy server if necessary
    //proxyAuth: 'user:password',

    // Header fields for proxy server if necessary
    //headers: {
    //  'User-Agent': 'Node'
    //}
  }
});

var options = {
  host: 'xsa.kontainerdog.com',
  port: 80,
  path: '/',
  agent: tunnelingAgent,
  headers: {
   'User-Agent': 'Node'
  }
};

callback = function(response) {
  var str = '';

  //another chunk of data has been recieved, so append it to `str`
  response.on('data', function (chunk) {
    str += chunk;
  });

  //the whole response has been recieved, so we just print it out here
  response.on('end', function () {
    console.log(str);
  });
}

http.request(options, callback).end();
